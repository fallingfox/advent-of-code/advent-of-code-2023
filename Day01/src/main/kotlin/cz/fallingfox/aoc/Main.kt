package cz.fallingfox.aoc

import java.io.File

val textDigits = listOf("one", "two", "three", "four", "five", "six", "seven", "eight", "nine")

fun main() {
    val input = loadInput("input.txt")
    input.map { replaceFirstTextDigit(it) }.map { replaceLastTextDigit(it) }.sumOf { findDigit(it) }.let { println(it) }
//    input.map { replaceFirstTextDigit(it) }.map { replaceLastTextDigit(it) }.forEach(::println)
}

fun findDigit(line: String): Int {
    val first = line.first { it.isDigit() }.toString()
    val last = line.last { it.isDigit() }.toString()
    return (first + last).toInt()
}

fun replaceFirstTextDigit(line: String): String {
    val indexes = textDigits.map { line.indexOf(it) }.map { if (it == -1) Int.MAX_VALUE else it }
    val minIndex = indexes.min()
    val digitIndex = indexes.indexOf(minIndex)
    if (minIndex == Int.MAX_VALUE) return line
    return line.substring(0, minIndex) + (digitIndex + 1) + line.substring(minIndex)
}

fun replaceLastTextDigit(line: String): String {
    val indexes = textDigits.map { line.lastIndexOf(it) }
    val maxIndex = indexes.max()
    val digitIndex = indexes.indexOf(maxIndex)
    if (maxIndex == -1) return line
    return line.substring(0, maxIndex + textDigits[digitIndex].length) + (digitIndex + 1) + line.substring(maxIndex + textDigits[digitIndex].length)
}

fun loadInput(filename: String): List<String> {
    return File(filename).readLines()
}