plugins {
    kotlin("jvm") version "1.9.21"
    application
}

group = "cz.fallingfox"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

application {
    mainClass.set("cz.fallingfox.aoc.MainKt")
}

dependencies {
    testImplementation(kotlin("test"))
}

tasks.test {
    useJUnitPlatform()
}

kotlin {
    jvmToolchain(8)
}