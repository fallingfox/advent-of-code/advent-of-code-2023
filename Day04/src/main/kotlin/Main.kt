import java.io.File

fun main() {
    partTwo()
}

fun partTwo() {
    val input = loadInput("input.txt")

    val mapOfCards = input.map(::parseCard).associateBy { it.id }
    val theCards = mapOfCards.values.toMutableList()

    var index = 0
    while (index < theCards.size) {
        val card = theCards[index]
        val newCards = findMatchedCards(card, mapOfCards)
        theCards.addAll(newCards)
        index++
    }

    println(theCards.size)
}

fun parseCard(line: String): Card {
    val id = line.substring(line.indexOf(" ") + 1, line.indexOf(":")).trim().toInt()
    val winningNumbers = line.substring(line.indexOf(":") + 1, line.indexOf("|")).split(" ").filter {
        it.isNotEmpty()
    }.map(String::toInt)
    val revealedNumbers = line.substring(line.indexOf("|") + 1).split(" ").filter {
        it.isNotEmpty()
    }.map(String::toInt)

    return Card(id, winningNumbers, revealedNumbers)
}

fun findMatchedCards(card: Card, mapOfCards: Map<Int, Card>): List<Card> {
    return card.winningNumbers.filter { card.revealedNumbers.contains(it) }.size.let { count ->
        val newCards = mutableListOf<Card>()
        for (i in 1 .. count) {
            newCards.add(mapOfCards.getValue(card.id + i))
        }
        newCards
    }
}

data class Card(
    val id: Int,
    val winningNumbers: List<Int>,
    val revealedNumbers: List<Int>,
    val matchedNumbers: MutableList<Int> = emptyList<Int>().toMutableList(),
)

fun loadInput(filename: String): List<String> {
    return File(filename).readLines()
}