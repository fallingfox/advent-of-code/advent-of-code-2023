import java.io.File

fun main() {
    val input = File("input.txt").readText()

    val result = solutionPartTwo(input)
    println("Result: $result")
}

fun solutionPartOne(input: String): Int {
    val times = parseTimes(input.lines().first()).apply(::println)
    val distances = parseDistances(input.lines().last()).apply(::println)
    val wins = mutableListOf<Int>()

    times.indices.forEach { index ->
        val time = times[index]
        val distance = distances[index]
        var numOfWins = 0

        var speed = 0
        while (speed < time) {
            val timeForRide = time - speed
            if ((timeForRide * speed) > distance) {
                numOfWins++
            }
            speed++
        }
        wins.add(numOfWins)
    }

    var sum = 1
    wins.forEach { sum *= it }

    return sum
}

fun solutionPartTwo(input: String): Long {
    val time = parseTime(input.lines().first()).apply(::println)
    val distance = parseDistance(input.lines().last()).apply(::println)

    var wins = 0L
    var speed = 0L
    while (speed < time) {
        val timeForRide = time - speed
        if ((timeForRide * speed) > distance) {
            wins++
        }
        speed++
    }
    return wins
}

fun parseTimes(line: String): List<Int> {
    return line.replace("Time: +".toRegex(), "").replace(" +".toRegex(), " ")
        .split(" ").map { it.toInt() }
}

fun parseTime(line: String): Long {
    return line.replace("Time: +".toRegex(), "").replace(" +".toRegex(), "").toLong()
}

fun parseDistances(line: String): List<Int> {
    return line.replace("Distance: +".toRegex(), "").replace(" +".toRegex(), " ")
        .split(" ").map { it.toInt() }
}

fun parseDistance(line: String): Long {
    return line.replace("Distance: +".toRegex(), "").replace(" +".toRegex(), "").toLong()
}