package cz.fallingfox.aoc

import java.io.File

fun main() {
    loadIntput("input.txt").map { parseGame(it) }.onEach { game ->
        println("Game ${game.id}: ${game.red} red, ${game.green} green, ${game.blue} blue, ${game.red * game.green * game.blue} power")
    }.sumOf { it.red * it.green * it.blue }.let { println("Total power: $it") }
}

data class Game(
    val id: Int,
    var red: Int,
    var green: Int,
    var blue: Int
)

fun parseGame(line: String): Game {
    val gameId = line.substring(5, line.indexOf(':')).toInt()

    val game = Game(gameId, 0, 0, 0)
    line.substring(line.indexOf(':') + 2).split(";").forEach{ draw ->
        draw.split(",").forEach { cubes ->
            val (count, color) = cubes.trim().split(" ")
            when (color) {
                "red" -> if (game.red < count.toInt()) game.red = count.toInt()
                "green" -> if (game.green < count.toInt()) game.green = count.toInt()
                "blue" -> if (game.blue < count.toInt()) game.blue = count.toInt()
            }
        }
    }

    return game
}

fun loadIntput(filename: String): List<String> {
    return File(filename).readLines()
}