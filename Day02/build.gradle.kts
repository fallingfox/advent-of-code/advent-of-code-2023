import org.jetbrains.kotlin.utils.addToStdlib.applyIf

plugins {
    kotlin("jvm") version "1.9.21"
    application
}

group = "cz.fallingfox"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    testImplementation(kotlin("test"))
}

application {
    mainClass = "cz.fallingfox.aoc.MainKt"
}

tasks.test {
    useJUnitPlatform()
}

kotlin {
    jvmToolchain(8)
}