import java.io.File

fun main() {
    partTwo("input.txt")
}

fun partOne(filename: String) {
    val input = loadInput(filename)

    val parts = mutableListOf<Int>()

    var strNumber = ""
    val rows = input
    for (y in rows.indices) {
        val row = rows[y]
        for (x in row.indices) {
            val column = row[x]

            if (column.isDigit()) {
                strNumber += column.toString()
            }

            if ((x + 1 == row.length || !row[x + 1].isDigit()) && strNumber.isNotEmpty()) {
                val theNumber = strNumber.toInt()

                var startX = x - strNumber.length
                if (startX < 0) {
                    startX = 0
                }
                var endX = x + 1
                if (endX >= row.length) {
                    endX = x
                }

                var startY = y - 1
                if (startY < 0) {
                    startY = 0
                }

                var endY = y + 1
                if (endY >= rows.size) {
                    endY = y
                }

                var isPart = false
                for (actualY in startY..endY) {
                    for (actualX in startX..endX) {
                        val theChar = rows[actualY][actualX]

                        if (!theChar.isDigit() && theChar != '.') {
                            isPart = true
                        }
                    }
                }

                if (isPart) {
                    parts.add(theNumber)
                }

                strNumber = ""
            }
        }
    }

    parts.onEach(::println).sum().let(::println)
}

fun partTwo(filename: String) {
    val input = loadInput(filename)

    val parts = getParts(input)
    val gears = getGears(input)
    val ratios = getRatiosOfGears(gears, parts)

    ratios.sum().let(::println)
}

fun getParts(input: List<String>): HashMap<Pair<Int, Int>, Int> {
    val parts = hashMapOf<Pair<Int, Int>, Int>()

    var strNumber = ""
    val rows = input
    for (y in rows.indices) {
        val row = rows[y]
        for (x in row.indices) {
            val column = row[x]

            if (column.isDigit()) {
                strNumber += column.toString()
            }

            if ((x + 1 == row.length || !row[x + 1].isDigit()) && strNumber.isNotEmpty()) {
                val theNumber = strNumber.toInt()

                val startX = x - strNumber.length + 1
                val endX = x
                val startY = y
                val endY = y

                for (actualY in startY..endY) {
                    for (actualX in startX..endX) {
                        val position = Pair(actualX, actualY)
                        parts[position] = theNumber
                    }
                }

                strNumber = ""
            }
        }
    }
    return parts
}

fun getGears(input: List<String>): List<Pair<Int, Int>> {
    val gears = mutableListOf<Pair<Int, Int>>()

    val rows = input
    for (y in rows.indices) {
        val row = rows[y]
        for (x in row.indices) {
            val column = row[x]

            if (column == '*') {
                gears.add(Pair(x, y))
            }
        }
    }

    return gears
}

fun getRatiosOfGears(gears: List<Pair<Int, Int>>, parts: HashMap<Pair<Int, Int>, Int>): List<Int> {
    return gears.map { (gearX, gearY) ->
        val pickedParts = hashMapOf<Pair<Int, Int>, Int>()

        val startX = gearX - 1
        val endX = gearX + 1

        val startY = gearY - 1
        val endY = gearY + 1

        for (actualY in startY..endY) {
            for (actualX in startX..endX) {
                val position = Pair(actualX, actualY)

                if (parts.contains(Pair(actualX, actualY))) {
                    pickedParts[position] = parts.getValue(position)
                }
            }
        }

        if (pickedParts.contains(Pair(gearX, gearY - 1))) {
            pickedParts.remove(Pair(gearX + 1, gearY - 1))
            pickedParts.remove(Pair(gearX - 1, gearY - 1))
        }

        if (pickedParts.contains(Pair(gearX, gearY + 1))) {
            pickedParts.remove(Pair(gearX + 1, gearY + 1))
            pickedParts.remove(Pair(gearX - 1, gearY + 1))
        }


        var ratio = 0
        if (pickedParts.size == 2) {
            ratio = 1
            pickedParts.values.forEach {
                ratio *= it
            }
        }

        ratio
    }
}


fun loadInput(filename: String): List<String> {
    return File(filename).readLines()
}