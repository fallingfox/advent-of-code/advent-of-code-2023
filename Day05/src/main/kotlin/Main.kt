import java.io.File

fun main() {
    println("Hello day 5 of Advent of Code 2023!")
    println("Part 2")

    val theInput = loadInput("input.txt")
    println("Solution: ${solution(theInput)}")
}

fun solution(input: List<String>): Long {
    val seeds = parseSeeds(input)
    val maps = parseMaps(input)

    var minValue = Long.MAX_VALUE

    var seedNum = 1
    seeds.forEach { range ->
        println("Seed ${seedNum++}")
        range.forEach { seed ->
            var processedSeed = seed
            maps.forEach { map ->
                map.firstOrNull { it.first.contains(processedSeed) }?.let { (source, destination) ->
                    val offset = processedSeed - source.first
                    processedSeed = destination.first + offset
                }
            }

            if (processedSeed < minValue) {
                minValue = processedSeed
            }
        }
    }

    return minValue
}

fun parseSeeds(input: List<String>): List<LongRange> {
    val nums = input.first().removePrefix("seeds: ").split(" ").filter { it.isNotEmpty() }
        .map { it.trim().toLong() }

    val seeds = mutableListOf<LongRange>()

    var index = 0
    while (index < nums.size) {
        val beginning = nums[index]
        val end = beginning + nums[index + 1]

        (beginning..<end).let(seeds::add)

        index += 2
    }

    return seeds
}

fun parseMaps(input: List<String>): List<List<Pair<LongRange, LongRange>>> {
    val mapLines: MutableList<String> = mutableListOf()
    val maps: MutableList<List<Pair<LongRange, LongRange>>> = mutableListOf()
    input.drop(3).forEach {
        if (it.isNotEmpty() && it.first().isDigit()) {
            mapLines.add(it)
        } else if (mapLines.isNotEmpty()) {
            maps.add(parseMap(mapLines))
            mapLines.clear()
        }
    }
    if (mapLines.isNotEmpty()) {
        maps.add(parseMap(mapLines))
    }
    return maps
}

fun parseMap(linesOfMap: List<String>): List<Pair<LongRange, LongRange>> {
    return linesOfMap.map {
        val data = it.split(" ")
        val destinationStart = data[0].toLong()
        val sourceStart = data[1].toLong()
        val count = data[2].toLong()

        val source = (sourceStart..<sourceStart + count)
        val desctination = destinationStart..<destinationStart + count
        Pair(source, desctination)
    }
}

fun loadInput(filename: String): List<String> {
    return File(filename).readLines()
}