/*
 * This Kotlin source file was generated by the Gradle 'init' task.
 */
package cz.fallingfox.aoc

import java.io.File

val cardTypes = listOf('J', '2', '3', '4', '5', '6', '7', '8', '9', 'T', 'Q', 'K', 'A')

val cardsCombinations = listOf("11111", "2111", "221", "311", "32", "41", "5")

typealias Card = Char

class Hand(
    val cards: List<Card>,
    val bid: Int,
    var combination: String = "",
    var combinationStrength: Int = 0,
    var rank: Int = 0,
    var prize: Int = 0
) : Comparable<Hand> {
    override fun compareTo(other: Hand): Int {
        if (combinationStrength > other.combinationStrength) {
            return 1
        } else if (combinationStrength < other.combinationStrength) {
            return -1
        } else {
            for (index in cards.indices) {
                val aIndex = cardTypes.indexOf(cards[index])
                val bIndex = cardTypes.indexOf(other.cards[index])
                val comp = aIndex.compareTo(bIndex)
                if (comp != 0) return comp
            }
            return 0
        }
    }
}

fun main() {
    loadRealInput().let(::solution).let { println("Solution: $it") }
}

fun solution(input: String): Int {
    return input.lines().map(::parseHand).onEach {
        it.combination = getCombination(it.cards)
        it.combinationStrength = getCombinationRank(it)
    }.sorted().onEachIndexed { index, hand ->
        hand.rank = index + 1
        hand.prize = hand.bid * hand.rank
    }.onEach { println("${it.rank} ${it.cards} ${it.combination} ${it.bid} ${it.prize}") }.sumOf { it.prize }
}

fun getCombination(cards: List<Card>) = cardTypes.map { type -> cards.count { card -> card == type } }.let {
        val js = it.first()
        it.drop(1).toMutableList().apply { this[this.indexOf(this.max())] += js }
    }.filter { it != 0 }
        .sortedDescending().joinToString("")

fun getCombinationRank(hand: Hand) = (cardsCombinations.indexOf(hand.combination) + 1)

fun parseHand(line: String) = line.split(" ").let { Hand(it.first().toList(), it.last().toInt()) }
fun loadInput(filename: String) = File(filename).readText()
fun loadTestInput() = loadInput("inputTest.txt")
fun loadRealInput() = loadInput("input.txt")